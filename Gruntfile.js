'use strict';

module.exports = function (grunt) {

    require('jit-grunt')(grunt);

    grunt.initConfig({
        babel: {
            options: {
                sourceMap: true,
                presets: ['react', 'es2015']
            },
            dist: {
                files: {
                    'app/js/app.ES5.js': 'app/js/app.js'
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    'app/js/app.min.js': 'app/js/app.ES5.js'
                }
            }
        }
    });

    grunt.registerTask('default', ['babel', 'uglify']);

};